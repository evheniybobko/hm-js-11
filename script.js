const passIcon = document.querySelector(".pass i");
const passConfirmIcon = document.querySelector(".pass-confirm i");

const passInput = document.querySelector(".pass input");
const passConfirmInput = document.querySelector(".pass-confirm input");

const submitBtn = document.querySelector(".btn");

const passDontMatch = document.querySelector(".form_pass-dont-match");


passIcon.addEventListener("click", () => {
  passIcon.classList.toggle("fa-eye-slash");
    if(passIcon.classList.contains("fa-eye-slash")) {
      passInput.setAttribute("type", "text");
    } else {
      passInput.setAttribute("type", "password");
    }
})

passConfirmIcon.addEventListener("click", () => {
  passConfirmIcon.classList.toggle("fa-eye-slash");
    if(passConfirmIcon.classList.contains("fa-eye-slash")) {
      passConfirmInput.setAttribute("type", "text");
    } else {
      passConfirmInput.setAttribute("type", "password");
    }    
})


submitBtn.addEventListener("click", (event) => {
  event.preventDefault();
  if(passInput.value === passConfirmInput.value) {
    alert("You are welcome");
  } else {
    passDontMatch.style.display = "block";
    passInput.addEventListener("focus", () => {
      passDontMatch.style.display = "none";
    })
    passConfirmInput.addEventListener("focus", () => {
        passDontMatch.style.display = "";
    
    })
  }
})



